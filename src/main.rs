#[macro_use]
extern crate serde_derive;

use std::io;
use std::process;
use std::io::Write;

mod blockchain;

fn main() {
    let mut miner_addr = String::new();
    let mut difficulty = String::new();
    let mut choice = String::new();

    // Beginning of CLI prompt:
    print!("Welcome! To get started, please input a miner address: ");
    io::stdout().flush();
    io::stdin().read_line(&mut miner_addr);
    print!("Next, set the difficulty level. Keep in mind that a higher difficulty level will have a greater impact on CPU performance (recommended 0-4): ");
    io::stdout().flush();
    io::stdin().read_line(&mut difficulty);
    let diff = difficulty.trim().parse::<u32>().expect("Apologies, but only integers are acceptable.");
    println!("Thank you. Please wait while I generate the genesis block!");
    let mut chain = blockchain::Chain::new(miner_addr.trim().to_string(), diff);

    loop {
        println!("Actions Menu");
        println!("1) Make a transaction.");
        println!("2) Mine a block.");
        println!("3) Change difficulty.");
        println!("4) Change reward.");
        println!("0) Exit");
        print!("Enter action number: ");
        io::stdout().flush();
        choice.clear();
        io::stdin().read_line(&mut choice);
        println!("");

        match choice.trim().parse().unwrap() {
            0 =>
            {
                println!("exiting!");
                process::exit(0);
            },
            1 => {
                let mut sender = String::new();
                let mut receiver = String::new();
                let mut amount = String::new();

                print!("Please enter the address of the sender:");
                io::stdout().flush();
                io::stdin().read_line(&mut sender);
                print!("Please enter the address of the receiver:");
                io::stdout().flush();
                io::stdin().read_line(&mut receiver);
                print!("Please enter the amount that will be sent:");
                io::stdout().flush();
                io::stdin().read_line(&mut amount);

                // result:
                let res = chain.new_transaction(sender.trim().to_string(), 
                                        receiver.trim().to_string(), 
                                        amount.trim().parse().unwrap());

                match res {
                    true => println!("Transaction has been added."),
                    false => println!("Apologies, but the transaction has failed"),
                }
            },
            2 =>
            {
                println!("Generating block...");
                let res = chain.generate_new_block();
                match res {
                    true => println!("Successfully generated block!"),
                    false => println!("Failed to generate block."),
                }
            },
            3 =>
            {
                let mut new_diff = String::new();
                print!("Please enter new difficulty level: ");
                io::stdout().flush();
                io::stdin().read_line(&mut new_diff);
                let res = chain.update_difficulty(new_diff.trim().parse().unwrap());
                match res {
                    true => println!("Updated difficulty level."),
                    false => println!("Failed to update difficulty"),
                }
            },
            4 =>{
                let mut new_reward = String::new();
                print!("Please enter a new reward: ");
                io::stdout().flush();
                io::stdin().read_line(&mut new_reward);
                let res = chain.update_reward(new_reward.trim().parse().unwrap());
                match res {
                    true => println!("Updated reward"),
                    false => println!("Failed Update reward"),
                }
            }
            _ => println!("Invalid option please retry"),
        }

    }
}
